package com.androidegitim.yeniekran;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class ThirdActivity extends BaseActivity {

    public final static String DATA = "LAHANA";

    @BindView(R.id.third_textview_data)
    TextView dataTextView;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_third);

        String data = getIntent().getExtras().getString(DATA);

        dataTextView.setText(data);

    }
}
