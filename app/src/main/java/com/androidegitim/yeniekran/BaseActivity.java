package com.androidegitim.yeniekran;

import android.app.Activity;
import android.os.Bundle;

import butterknife.ButterKnife;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public abstract class BaseActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState, int layoutID) {
        super.onCreate(savedInstanceState);

        setContentView(layoutID);

        ButterKnife.bind(this);
    }
}
