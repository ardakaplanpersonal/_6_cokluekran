package com.androidegitim.yeniekran;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;

public class FirstActivity extends BaseActivity {

    @BindView(R.id.first_edittext_third_activity_data)
    EditText thirdActivityDataEditText;


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_first);


    }


    @OnClick(R.id.first_button_start_second_activity)
    public void openSecondActivity() {

//        findViewById(R.id.main_button_start_second_activity).setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
//            }
//        });


        Intent intent = new Intent(this, SecondActivity.class);
//        Intent intent = new Intent(getApplicationContext(), SecondActivity.class);

        startActivity(intent);

    }

    @OnClick(R.id.first_button_start_third_activity)
    public void openThirdActivity() {

        Intent intent = new Intent(this, ThirdActivity.class);

        intent.putExtra(ThirdActivity.DATA, thirdActivityDataEditText.getText().toString());

        startActivity(intent);
    }
}
