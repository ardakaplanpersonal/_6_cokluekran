package com.androidegitim.yeniekran;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class SecondActivity extends BaseActivity {


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_second);
    }
}
